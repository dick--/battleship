var io = require('socket.io').listen(8080);

var usernames = {};
/* usernames values = username state
	-started - placing ships,
	-ready - shooting enemy,
	-win/loose - finished state with result
*/	
var rooms = {};

io.sockets.on('connection', function (socket) {
	socket.on('adduser', function(username){
		socket.username = username;
		rooms[username] = username + ' game';
		socket.room = rooms[username];
		usernames[username] = 'started';
		socket.join(rooms[username]);
		socket.emit('setRoom', socket.room);
		socket.emit('updatechat', '(SERVER)', 'you have connected to '+rooms[username]);
		socket.broadcast.to(rooms[username]).emit('updatechat', '(SERVER)', username + ' has connected to this room');
	});
	socket.on('refreshRoomsSend', function () {
		socket.emit('refreshRoomsRecieve', rooms);
	});
	socket.on('sendchat', function (data) {
		io.sockets.in(socket.room).emit('updatechat', socket.username, data);
	});
	socket.on('shot', function (position,user) {
		io.sockets.in(socket.room).emit('recieveShot', position, user);
	});
	socket.on('shotBack', function (position,user,matrix) {
		io.sockets.in(socket.room).emit('shitBack', position, user, matrix);
	});
	socket.on('switchRoom', function(newroom, id) {
		socket.username = id;
		usernames[id] = 'started';
		socket.join(newroom);
		socket.room = newroom;
		socket.emit('updatechat', '(SERVER)', 'you have connected to '+ newroom + ' as '+ id);
		socket.emit('setRoom', socket.room);
		socket.broadcast.to(newroom).emit('updatechat', '(SERVER)', id+' has joined this room');
	});
	socket.on('disconnect', function() {
		delete usernames[socket.username];
		delete rooms[socket.username];
		io.sockets.emit('updateusers', usernames);
		socket.broadcast.emit('updatechat', '(SERVER)', socket.username + ' has disconnected');
		socket.leave(socket.room);
	});
});