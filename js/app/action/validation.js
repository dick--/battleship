define(function(){

	function validationX (matrix,position,size) {
		var result = true;
		if ((parseInt(position['x'])<0) || (parseInt(position['x'])+parseInt(size)-1>9) || (parseInt(position['y'])<0) || (parseInt(position['y'])>9)) {
			result = false;
		} else {
			for (var i = parseInt(position['x'])-1; i < parseInt(position['x'])+parseInt(size)+1; i++) {
				for (var j = parseInt(position['y'])-1; j <= parseInt(position['y'])+1; j++) {
					if ((j < 0) || (i < 0) || (j>9) || (i>9)) {
						continue;
					} else {
						(matrix[j][i] != 0) ? result = false : result;
					}
				};
			};
		}
		return result;
	};

	function validationY (matrix,position,size) {
		var result = true;
		if ((parseInt(position['x'])<0) || (parseInt(position['x'])>9) || (parseInt(position['y'])<0) || (parseInt(position['y'])+parseInt(size)-1>9)) {
			result = false;
			return result;
		} else {
			for (var i = parseInt(position['x'])-1; i <= parseInt(position['x'])+1; i++) {
				for (var j = parseInt(position['y'])-1; j < parseInt(position['y'])+parseInt(size)+1; j++) {
					if ((j < 0) || (i < 0) || (j>9) || (i>9)) {
						continue;
					} else {
						(matrix[j][i] != 0) ? result = false : result;
					}
				};
			};
		}
		return result;
	};

	return {
		valX: validationX,
		valY: validationY
	}
});