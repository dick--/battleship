define(['app/ui/buttons','app/ui/console','app/ui/tables','app/model/ship','action/validation'], function (buttons,console,tables,Ship,validation){

	function listenBtns (configObj) {
		$(configObj.stuff['reverse']).on('click', function () {
			$(this).toggleClass('vertical');
		});
		$('.controls').on('click','input', function () {
			var index = $(this).index()+1; // index() counts from 0;
			var position = {
				'x' : '',
				'y' : ''
			};
			var ship = new Ship(configObj,configObj.maxCount[index]);
			buttons.refresh(configObj);
			if (buttons.refresh(configObj)) {
				console.message('(Battle log): All ships are placed! Checking your opponent...',configObj);
				
			}
			$('.controls input').attr('disabled','disable');

			$(configObj.tableSelector[1]+' td').one('click', function () {
				var vertical;
				position['x'] = $(this).index();
				position['y'] = $(this).parent().index();
				($(configObj.stuff["reverse"]).hasClass('vertical')) ? vertical=1 : vertical=0;
				if (vertical) {
					if (validation.valY(tables.getMatrix(configObj,1),position,ship.size)) {
						$(configObj.tableSelector[1]).html(tables.setMatrix(ship.place(tables.getMatrix(configObj,1),position,vertical)));
						console.message('(Battle log): Successfully placed ' + ship.size + ' squared ship!',configObj);
						$('.controls input').removeAttr('disabled');
						buttons.refresh(configObj);
					} else {
						console.message('(Battle log): Invalid ship placement!',configObj);
					}
				} else {
					if (validation.valX(tables.getMatrix(configObj,1),position,ship.size)) {
						$(configObj.tableSelector[1]).html(tables.setMatrix(ship.place(tables.getMatrix(configObj,1),position,vertical)));
						console.message('(Battle log): Successfully placed ' + ship.size + ' squared ship!',configObj);
						$('.controls input').removeAttr('disabled');
						buttons.refresh(configObj);
					} else {
						console.message('(Battle log): Invalid ship placement!',configObj);
					}
				}
			});
		});
	};

	return {
		start : listenBtns
	}
});