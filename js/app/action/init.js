define(['action/listen','app/ui/console','app/ui/tables'], function (listen,console,tables) {

	function init (configObj) {
		$(configObj.tableSelector[1]).html(tables.render(configObj));
		$(configObj.tableSelector[2]).html(tables.render(configObj));
		listen.start(configObj);
		$(configObj.stuff['dimensionForm']).hide();
		$(configObj.stuff['fieldToHide']).hide();
		$(configObj.stuff['controlsBlock']).show();
		$(configObj.stuff['hiddenFields']).show();
		console.message('(Battle log): Set up your ships',configObj)
	};

	return {
		init : init
	};

});