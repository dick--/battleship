define(['app/ui/console','app/ui/tables','action/init'], function(output,tables,init){
    var currentRoom;
    var id;

    function start(configObj,socket) {

        function switchRoom(room,id) {
            socket.emit('switchRoom', room, id);
        };

        function detectShot (cfg) {
            var position = {
                'x' : '',
                'y' : ''
            };
            $('body').on('click',cfg.tableSelector[2]+' td', function () {
                position['y'] = $(this).index();
                position['x'] = $(this).parent().index();
                socket.emit('shot',position,id);
            });
        };

        $(configObj.stuff['refresh']).on('click', function() {
            socket.emit('refreshRoomsSend');
        });

        socket.on('refreshRoomsRecieve', function(rooms) {
            $(configObj.stuff['hostList'] + ' ul').empty();
            for (var prop in rooms) {
                $(configObj.stuff['hostList'] + ' ul').append('<li><a href="#" data-room="'+rooms[prop]+'">' + rooms[prop] + '</a></li>');
            }
        });

        socket.on('setRoom', function(newroom) {
            currentRoom = newroom;
        });

        socket.on('recieveShot', function(position,user) {
            if (id == user) {
            } else {
                socket.emit('shotBack', position, id, tables.getMatrix(configObj, 1));
                $(configObj.tableSelector[1]).html(tables.getShot(position,configObj));
                
            }
        });

        socket.on('shitBack', function(position,user,matrix) {
            if (id == user) {

            } else {
                $(configObj.tableSelector[2]).html(tables.shotBack(matrix,configObj,position));
            }
        });

        detectShot(configObj);

        $(configObj.stuff['hostList']).on('click','a',function () {
            id = $(configObj.stuff['name']).val();
            if (id == '') {
                $(configObj.stuff['err']).show();
            } else {
                $(configObj.stuff['err']).hide();
                
                init.init(configObj);
                switchRoom($(this).data("room"),id);
                
                socket.on('updatechat', function (username, data) {
                    output.message(username+': '+data,configObj);
                });
                
                $(configObj.stuff['mesInput']).on('keypress', function(e) {
                    if (e.which == '13') {
                        socket.emit('sendchat', $(configObj.stuff['mesInput']).val());
                        $(configObj.stuff['mesInput']).val('');
                    }
                });
            }
        });

        $(configObj.stuff['dimensionForm']).on('click', 'input[value="create"]', function() { 
            id = $(configObj.stuff['name']).val();
            if (id == '') {
                $(configObj.stuff['err']).show();
            } else {
                init.init(configObj);
                $(configObj.stuff['err']).hide();
                socket.emit('adduser',id);
                
                socket.on('updatechat', function (username, data) {
                    output.message(username+': '+data,configObj);
                });
                
                $(configObj.stuff['mesInput']).on('keypress', function(e) {
                    if (e.which == '13') {
                        socket.emit('sendchat', $(configObj.stuff['mesInput']).val());
                        $(configObj.stuff['mesInput']).val('');
                    }
                });
            };
        });
    }

    return {
        start : start
    }
});