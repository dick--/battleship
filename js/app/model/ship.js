define(function(){

    function Ship (configObj,size) {
		this.size = size;
		configObj.actualCount[size]++;
	};

	Ship.prototype.place = function(matrix,position,reverse) {
		if (reverse == 0) {
			for (var i = position['x']; i < parseInt(position['x'])+parseInt(this.size); i++) {
				matrix[position['y']][i] = 2;
			};
		} else {
			for (var i = position['y']; i < parseInt(position['y'])+parseInt(this.size); i++) {
				matrix[i][position['x']] = 2;
			};
		}
		return matrix;
	};

    return Ship;
    
});