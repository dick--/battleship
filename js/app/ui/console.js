define(function(){

	function postMessage (message,configObj) {
		var consoleCurr = $(configObj.stuff['console']).html();
		var result = consoleCurr + '\n' + message;
		return (function () {
				$(configObj.stuff['console']).html(result);
				$(configObj.stuff['console']).scrollTop($(configObj.stuff['console'])[0].scrollHeight-$(configObj.stuff['console']).height());
			})();
	};

	return {
		message : postMessage
	}

});