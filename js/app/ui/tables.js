define(function(){

    function renderTables (configObj) {
        var result='';
		var dimensionX = $(configObj.dimensionSel['x']).val();
		var dimensionY = $(configObj.dimensionSel['y']).val();
		configObj.dimensionX = dimensionX;
		configObj.dimensionY = dimensionY;
		for (var i = 1; i <= dimensionX; i++) {
			result +='<tr>'
			for (var j = 1; j <= dimensionY; j++) {
				result += '<td></td>'
			};
			result +='</tr>'
		};
		return result;
    };

    function getMatrix (configObj,num) {
		var tabletr = $(configObj.tableSelector[num]).find('tr');
		var matrix = new Array(tabletr.length);
		for (var i = 0; i < tabletr.length; i++) {
			matrix[i] = new Array($(tabletr[i]).find('td').length);
		};
		for (var i = 0; i < tabletr.length; i++) {
			for (var j = 0; j < $(tabletr[i]).find('td').length; j++) {
				if ($($(tabletr[i]).find('td')[j]).hasClass('ship hit')) {
					matrix[i][j] = 1;
				} else {
					if ($($(tabletr[i]).find('td')[j]).hasClass('ship')) {
						matrix[i][j] = 2;
					} else {
						if ($($(tabletr[i]).find('td')[j]).hasClass('miss')) {
							matrix[i][j] = 3;
						} else {
							matrix[i][j] = 0;
						}
					}
				};
			};
		};
		return matrix;
	};

	function setMatrix (matrix) {
		var result = '';
		for (var i = 0; i < matrix.length; i++) {
			result += '<tr>';
			for (var j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == 2) {
					result += '<td class="ship"></td>';
				} else {
					if (matrix[i][j] == 1) {
						result += '<td class="ship hit"></td>';
					} else {
						if (matrix[i][j] == 3) {
							result += '<td class="miss"></td>';
						} else {
							result += '<td></td>'
						}
					}
				}
			};
			result += '</tr>'
		};
		return result;
	};

	function getShot (pos,configObj) {
		var matrixLeft = getMatrix(configObj, 1);
		var x = pos['x'];
		var y = pos['y'];
		if ((matrixLeft[x][y] == 0) || (matrixLeft[x][y] == 3)) {
			matrixLeft[x][y] = 3;
		} else {
			matrixLeft[x][y] = 1;
		};
		return setMatrix(matrixLeft);			
	};
	function shotBack (matrixOrigin,configObj,pos) {
		var matrixRight = getMatrix(configObj, 2);
		var x = pos['x'];
		var y = pos['y'];
		if ((matrixOrigin[x][y] == 0) || (matrixOrigin[x][y] == 3))  {
			matrixRight[x][y] = 3;
		} else {
			matrixRight[x][y] = 1;
		};
		return setMatrix(matrixRight);
	}

    return {
    	render : renderTables,
    	getMatrix : getMatrix,
    	setMatrix : setMatrix,
    	getShot : getShot,
    	shotBack : shotBack
    }
});