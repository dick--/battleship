define(function(){
	
    function refresh (configObj) {
		for (var i = 1; i <= configObj.count; i++) {
			$(configObj.addbtnsSelectors[i]).val(i+'x' + (parseInt(configObj.maxCount[i])-parseInt(configObj.actualCount[i])));
		};
		disable();
		return (disable()) ? true : false;
	};

	function disable () {
		$('.controls input[value*="x0"]').attr('disabled','disable');
		return ($('.controls input[value*="x0"]').length == 4) ? true : false;
	};

    return {
    	refresh : refresh,
    	disable : disable
    }

});