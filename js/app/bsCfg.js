define(function(){

	var config = {
		count : 4,
		dimensionX : '',
		dimensionY : '',
		actualCount : {
			1 : "0",
			2 : "0",
			3 : "0",
			4 : "0"
		},
		maxCount : {
			1 : "4",
			2 : "3",
			3 : "2",
			4 : "1"
		},
		addbtnsSelectors : {
			"1" : "#ship1squared",
			"2" : "#ship2squared",
			"3" : "#ship3squared",
			"4" : "#ship4squared"
		},
		tableSelector : {
			1 : "#table1",
			2 : "#table2"
		},
		dimensionSel : {
			'x' : '#dimensionX',
			'y' : '#dimensionY'
		},
		stuff : {
			"reverse" : "#reverseBtn",
			"console" : "#console",
			"dimensionForm" : "#dimensionForm",
			"controlsBlock" : ".controls",
			"hiddenFields" : ".hidden",
			"fieldToHide" : ".tohide",
			"refresh" : "#refresh",
			"hostList" : ".hostedGames",
			"name" : "#name",
			"err" : ".err",
			"mesInput": "#messageInput"
		}
	};

	function setcfg (field,value) {
		config[field] = value;
	};

	function getcfg (field) {
		return config[field];
	};

	return {
		setcfg : setcfg,
		getcfg : getcfg,
		cfg : config
	};

});