requirejs.config({
    baseUrl: 'js/lib',
    paths: {
    	action: '../app/action',
        app: '../app',
        socketio: 'http://localhost:8080/socket.io/socket.io'
    }
});
//init:
requirejs(['jquery','socketio','app/bsCfg','action/client'], function($,io,bsCfg,client) {
		var config = bsCfg.cfg;
        var socket = io.connect('http://localhost:8080/');
        client.start(config,socket);
});