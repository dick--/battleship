Battleship game, made for Bionic University (homework)

uses SASS+Compass / require.js / socket.io / jquery

======

**to start server :**

in node shell or unix terminal: npm install && npm start 

**to start clients: **

just open battle.html in one or more tabs

